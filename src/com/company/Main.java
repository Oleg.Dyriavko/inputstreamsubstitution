package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        //кладем данные в строку
        StringBuilder sb = new StringBuilder();
        sb.append("Oleg").append('\n');
        sb.append("Julia").append('\n');
        sb.append("Artur").append('\n');
        sb.append("Myron").append('\n');
        String data = sb.toString();

        //Оборачиваем строку в класс ByteArrayInputStream
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());

        //подменяем in
        System.setIn(inputStream);

        //вызываем обычный метод, который не подозревает о наших манипуляциях
        readAndPrintLine();
    }

    public static void readAndPrintLine() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        while (true) {
            String line = bufferedReader.readLine();
            if (line == null) break;
            System.out.println(line);
        }
        bufferedReader.close();
        inputStreamReader.close();
    }

//      Чтоб переменная in брала данные обратно с консоли:

//    1) Сохранить изначальное значение переменной System.in
//       InputStream defaultInputStream = System.in;
//
//    2) Когда нужно будет вернуть обратно:
//       System.setIn(defaultInputStream);

}
